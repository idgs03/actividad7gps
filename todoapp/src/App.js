import React from 'react'
import './App.css'
import { Contador } from './components/Contador'
import { Buscador } from './components/Buscador'
import { Lista } from './components/Lista'
import { Item } from './components/Item'
import { Boton } from './components/Boton'


const tareas = [
  { text: 'Crear los componentes de la app', completed: false },
  { text: 'Tarea 2', completed: false },
  { text: 'Tarea 3', completed: false },
  { text: 'Tarea 4', completed: false },
]

function App() {
  return (
    <div className='row bgColorTable p-3 '>
      <div className='col-3'></div>
      <div className='col-6  bg-primary text-center'>
        <h1>To Do App</h1>
        <Contador />
        <Buscador />
        <Lista>
          {tareas.map(tarea => (

            <Item text={tarea.text} key={tarea.text} />

          ))}
        </Lista>
        <Boton />
      </div>
      <div className='col-3'></div>
    </div>
  );
}

export default App;
