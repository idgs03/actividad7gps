import React from 'react';
import './Lista.css';

function Lista(props) {
    return (
        <section className='container'>
            <ul>
                {props.children}
            </ul>
        </section>
    );
}

export { Lista };