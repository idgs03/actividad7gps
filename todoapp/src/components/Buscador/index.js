import React, { useState } from 'react'

function Buscador() {

    const [buscandoValor, setBuscandoValor] = useState("");

    const Buscar = (event) => {
        setBuscandoValor(event.target.value)
    }
    return (

        <div className="row m-4">
            <input class="form-control" type="text" placeholder="Buscar tarea" value={buscandoValor} aria-label="default input example"></input>
        </div>

    )
}

export { Buscador };