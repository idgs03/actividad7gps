import React from 'react'

function Boton() {
    return (
        <a class="waves-effect waves-light btn"><i class="material-icons left">add</i>Tarea</a>
    )
}

export { Boton };