import React from 'react';
import './item.css';

function Item(props) {
    return (

        <div className='row bg-ligh text-start t'>
            <div className='col-9  p-2'>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" />
                    <label class="form-check-label" for="flexCheckDefault">
                        {props.text}
                    </label>
                </div>
            </div>

            <div className='col-3'>
                <span>3
                </span>
            </div>
        </div>
    )
}

export { Item };